import { Module } from '@nestjs/common';
import { ContactModule } from './contact/contact.module';
import {AuthModule} from "./auth/auth.module";
import {ConfigModule, ConfigService} from "@nestjs/config";
import {TypegooseModule} from "nestjs-typegoose";
import {getMongoConfig} from "./configs/mongo.config";

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypegooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: getMongoConfig,
    }),
    AuthModule,
      ContactModule],
})
export class AppModule {}
