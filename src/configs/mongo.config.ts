import {ConfigService} from '@nestjs/config';
import {TypegooseModuleOptions} from 'nestjs-typegoose';

export const getMongoConfig = async (
    ConfigService: ConfigService,
): Promise<TypegooseModuleOptions> => {
    return {
        uri: getMongoUri(ConfigService),
        ...getMongoOptions(),
    };
};

const getMongoUri = (ConfigService: ConfigService) =>
    'mongodb+srv://' +
    ConfigService.get('MONGO_LOGIN') +
    ":" + ConfigService.get('MONGO_PASSWORD') +
    "@" + ConfigService.get('MONGO_HOST') +
    '/' + ConfigService.get('MONGO_DB');

const getMongoOptions = () => ({
    useNewUrlParser: true,
    // useCreateIndex: true,
    useUnifiedTopology: true,
});
