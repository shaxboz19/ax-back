import {
    BadRequestException,
    Body,
    Controller, Get,
    HttpCode,
    Post, UseGuards,
    UsePipes,
    ValidationPipe,
} from '@nestjs/common';
import { USER_ALREADY_EXISTS_ERROR } from './auth.constants';
import { AuthService } from './auth.service';
import { AuthDto } from './dto/auth.dto';
import {RegisterDto} from "./dto/register.dto";
import {JwtAuthGuard} from "./guards/jwt.guard";

@Controller('auth')
export class AuthController {

    constructor(private readonly authService: AuthService) {}

    @UsePipes(new ValidationPipe())
    @Post('register')
    async register(@Body() dto: RegisterDto) {
        const oldUser = await this.authService.findUser(dto.login);
        if (oldUser) {
            throw new BadRequestException(USER_ALREADY_EXISTS_ERROR);
        }
        return this.authService.createUser(dto);
    }

    @UsePipes(new ValidationPipe())
    @HttpCode(200)
    @Post('login')
    async login(@Body() { login, password }: AuthDto) {
        const data = await this.authService.validateUser(login, password);
        return this.authService.login(data);
    }

    @Get('check')
    @UseGuards(JwtAuthGuard)
    async check() {
        return {message: 'Ok'}
    }
}
