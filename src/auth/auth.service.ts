import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { compare, genSalt, hash } from 'bcryptjs';
import { InjectModel } from 'nestjs-typegoose';
import { EMAIL_NOT_FOUND_ERROR, WRONG_PASSWORD_ERROR } from './auth.constants';
import { AuthDto } from './dto/auth.dto';
import { UserModel } from './user.model';
import {RegisterDto} from "./dto/register.dto";

@Injectable()
export class AuthService {
    constructor(
        @InjectModel(UserModel) private readonly UserModel: ModelType<UserModel>,
        private readonly jwtService: JwtService,
    ) {}
    async createUser(dto: RegisterDto) {
        const salt = await genSalt(10);
        const passwordHash = await hash(dto.password, salt);
        const newUser = new this.UserModel({
            email: dto.login,
            passwordHash: passwordHash,
            name: dto.name,
        });

        return newUser.save();
    }

    async findUser(email: string) {
        return this.UserModel.findOne({ email }).exec();
    }

    async validateUser(email: string, password: string) {
        const user = await this.findUser(email);
        if (!user) {
            throw new UnauthorizedException(EMAIL_NOT_FOUND_ERROR);
        }
        const isSamePassword = await compare(password, user.passwordHash);
        if (!isSamePassword) {
            throw new UnauthorizedException(WRONG_PASSWORD_ERROR);
        }
        return { email, name: user.name };
    }

    async login(data) {
        return {
            access_token: await this.jwtService.signAsync(data),
        };
    }
}
