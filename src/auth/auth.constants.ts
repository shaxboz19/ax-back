export const USER_ALREADY_EXISTS_ERROR =
  'Пользователь с таким email уже зарегистрирован';
export const EMAIL_NOT_FOUND_ERROR = 'Пользователь с таким email не найден!';
export const WRONG_PASSWORD_ERROR = 'Неправильный пароль';
