import {prop} from "@typegoose/typegoose";

export class ContactModel {
    @prop()
    firstName: string;

    @prop()
    lastName: string;

    @prop()
    patronymic: string;

    @prop()
    phone: string;

    @prop()
    email: string;

    @prop({type: () => [String]})
    tags: string[];
}
