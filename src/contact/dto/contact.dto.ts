import {IsString} from "class-validator";

export class ContactDto {
    @IsString()
    firstName: string;

    @IsString()
    lastName: string;

    @IsString()
    patronymic: string;

    @IsString()
    phone: string;

    @IsString()
    email: string;

    @IsString()
    tags: string[];

}
