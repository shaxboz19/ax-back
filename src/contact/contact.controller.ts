import {Body, Controller, Delete, Get, Param, Patch, Post, Query} from '@nestjs/common';
import {ContactService} from "./contact.service";
import {ContactDto} from "./dto/contact.dto";

@Controller('contact')
export class ContactController {
    constructor(private readonly contactService: ContactService) {
    }
    @Get()
    async getAll(@Query() query) {
        return this.contactService.getAll(query)
    }

    @Get(':id')
    async getById(id: string) {
        return this.contactService.getById(id)
    }

    @Post()
    async create(@Body() dto: ContactDto) {
        console.log(dto)
        return this.contactService.create(dto)
    }

    @Patch(':id')
    async update(@Body() dto: ContactDto, @Param('id') id: string) {
        return this.contactService.update(id, dto)
    }

    @Delete(':id')
    async delete(@Param('id') id: string) {
        return this.contactService.delete(id)
    }

}
