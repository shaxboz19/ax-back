import {Injectable} from '@nestjs/common';
import {InjectModel} from "nestjs-typegoose";
import {ContactModel} from "./contact.model";
import {ModelType} from "@typegoose/typegoose/lib/types";

@Injectable()
export class ContactService {
    constructor(@InjectModel(ContactModel) private readonly contactModel: ModelType<ContactModel>) {
    }

    async getAll({ text, limit = 10, page = 1 }) {
        let filter = {};

        if (text) {
            filter = {
                $or: [
                    { firstName: { $regex: text, $options: 'i' } },
                    { lastName: { $regex: text, $options: 'i' } },
                    { phone: { $regex: text, $options: 'i' } },
                    { email: { $regex: text, $options: 'i' } },
                    { tags: { $regex: text, $options: 'i' } },

                ]
            }
        }
        const totalDocs = await this.contactModel.countDocuments(filter);
        const results = await this.contactModel
            .find(filter)
            .skip((page - 1) * limit)
            .limit(limit);

        return {
            docs: results,
            totalDocs,
            page,
        };
    }

    async getById(id: string) {
        return this.contactModel.findById(id).exec();
    }

    async create(dto) {
        return this.contactModel.create(dto);
    }

    async update(id: string, dto) {
        return this.contactModel.findByIdAndUpdate(id, dto, {new: true}).exec();
    }

    async delete(id: string) {
        return this.contactModel.findByIdAndDelete(id).exec();
    }

}
